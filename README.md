# Genotype Accuracy Estimation Based On Trios


This python script calculates the genotype accuracy using parent-offspring trio datasets. Genotype errors are identified as any variant call inconsistent with the rules of Mendelian inheritance, assuming that de novo mutation rates (~10−8 per bp) are several orders of magnitude smaller than calling and genotyping error rates (often 10−2−10−5⁠) (Kómár & Kural, 2018). Each variable position in a VCF file is classified as missing data, correct or incorrect genotype calling for each trio based on the offspring genotype assuming that the parent’s genotype were correct. It also assumes correctly specified familial relationships. Although genotype classification is defined based on the offspring genotype, the Mendelian error might have occurred in either the offspring or one or both parents. The script calculates the genotype error rate as the relationship between the number of correct and incorrect sites, discarding missing data. The script also removes the incorrect and missing genotypes and outputs a filtered VCF file.

## Running the script

Use the following command:

```
python3 GenotypeAccuracy.py ${fatherID} ${motherID} ${offspringID} ${VCF}
```
